valadoc (0.36.0-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * New upstream release
  * Add watch file
  * debian/control:
    - Build against vala 0.36
    - Require libgee >= 0.19.91
  * Drop patches:
    - fix-844761.patch: Fixed in libgee
    - update-manpage.patch: Applied in new release

 -- Sebastian Reichel <sre@debian.org>  Wed, 30 Aug 2017 21:45:28 +0200

valadoc (0.30.0~git20160518-2) unstable; urgency=medium

  * Import workaround for libgee bug (Closes: #844761)

 -- Sebastian Reichel <sre@debian.org>  Wed, 23 Nov 2016 18:39:10 +0100

valadoc (0.30.0~git20160518-1) unstable; urgency=medium

  * New upstream snapshot
  * Add support for vala 0.34 (Closes: #838334)
  * Switch to compat level 10
  * Update Debian Standards Version to 3.9.8
  * Update VCS urls
  * Drop cherry-picked patch from NMU (part of newer snapshot)
  * Update symbols for new upstream version
  * Enable hardening

 -- Sebastian Reichel <sre@debian.org>  Sun, 25 Sep 2016 11:11:55 +0200

valadoc (0.23.2~git20150422-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild against vala 0.32, cherry-picking the commit from
    upstream that enables it. (Closes: #819540)

 -- Tobias Frost <tobi@debian.org>  Sun, 29 May 2016 12:05:07 +0200

valadoc (0.23.2~git20150422-3) unstable; urgency=low

  * Refresh quilt patches
  * Add new patch updating manpage (Closes: #773989)

 -- Sebastian Reichel <sre@debian.org>  Fri, 06 Nov 2015 05:40:43 +0100

valadoc (0.23.2~git20150422-2) unstable; urgency=medium

  * Build for valac instead of valac-0.28 (Closes: #801239)

 -- Sebastian Reichel <sre@debian.org>  Tue, 03 Nov 2015 16:21:29 +0100

valadoc (0.23.2~git20150422-1) unstable; urgency=medium

  * New upstream snapshot
  * Add support for vala 0.28 (Closes: #788949)
  * Drop support for vala 0.26

 -- Sebastian Reichel <sre@debian.org>  Mon, 22 Jun 2015 01:08:28 +0200

valadoc (0.23.2~git20140902-3) unstable; urgency=low

  * Drop support for vala 0.24 (Closes: #764765)

 -- Sebastian Reichel <sre@debian.org>  Sat, 11 Oct 2014 03:13:04 +0200

valadoc (0.23.2~git20140902-2) unstable; urgency=low

  * Update copyright file (Closes: #760798)
  * Add support for vala 0.26
  * Update Debian Standards Version to 3.9.6

 -- Sebastian Reichel <sre@debian.org>  Fri, 10 Oct 2014 02:45:05 +0200

valadoc (0.23.2~git20140902-1) unstable; urgency=low

  * new upstream snapshot
   - support for gtkdoc markdown parser
  * Drop support for vala 0.16 (Closes: #755207)

 -- Sebastian Reichel <sre@debian.org>  Sun, 07 Sep 2014 22:29:40 +0200

valadoc (0.3.2~git20140325-1) unstable; urgency=low

  * new upstream snapshot
   - add support for vala 0.24
   - add support for vala 0.26
  * Drop support for vala 0.20 and 0.22
  * Enable support for vala 0.24 (Closes: #745417)

 -- Sebastian Reichel <sre@debian.org>  Tue, 13 May 2014 04:05:08 +0200

valadoc (0.3.2~git20140116-2) unstable; urgency=low

  * upload to unstable

 -- Sebastian Reichel <sre@debian.org>  Sun, 16 Feb 2014 14:32:49 +0100

valadoc (0.3.2~git20140116-1) experimental; urgency=low

  * new upstream snapshot
   - add support for vala 0.22 (Closes: #738312)
  * update Debian Standards Version to 3.9.4

 -- Sebastian Reichel <sre@debian.org>  Mon, 10 Feb 2014 11:14:50 +0100

valadoc (0.3.2~git20130319-2) unstable; urgency=low

  * Add missing Breaks/Replaces to libvaladoc-data (Closes: #713953)

 -- Sebastian Reichel <sre@debian.org>  Thu, 27 Jun 2013 17:48:43 +0200

valadoc (0.3.2~git20130319-1) unstable; urgency=low

  * new upstream snapshot
  * update Debian Standards Version to 3.9.4
  * drop support for vala 0.14 (Closes: #709728)
  * add support for vala 0.16 and 0.20
  * bump .so version number (API change)

 -- Sebastian Reichel <sre@debian.org>  Fri, 07 Jun 2013 20:10:50 +0200

valadoc (0.3.2~git20120227-1) unstable; urgency=low

  * new upstream snapshot
  * update Debian Standards Version to 3.9.3
  * update vala build dependency to 0.14 (Closes: #663325)
  * update debian/copyright to debian copyright format 1.0 format

 -- Sebastian Reichel <sre@debian.org>  Sat, 17 Mar 2012 05:54:07 +0100

valadoc (0.2+git20110728-3) unstable; urgency=low

  * fix syntax-error-in-dep5-copyright 

 -- Sebastian Reichel <sre@debian.org>  Wed, 14 Sep 2011 00:47:24 +0200

valadoc (0.2+git20110728-2) unstable; urgency=low

  * upload to unstable
  * add symbols file

 -- Sebastian Reichel <sre@debian.org>  Wed, 31 Aug 2011 02:04:41 +0200

valadoc (0.2+git20110728-1) experimental; urgency=low

  * Initial release (Closes: #582833)

 -- Sebastian Reichel <sre@debian.org>  Mon, 01 Aug 2011 20:56:39 +0200
