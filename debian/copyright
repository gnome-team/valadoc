Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: http://git.gnome.org/browse/valadoc/

Files: *
Copyright: 2008-2014 Florian Brosch <flo.brosch@gmail.com>
License: GPL-2

Files: debian/*
Copyright: 2011-2014 Sebastian Reichel <sre@debian.org>
License: ISC

Files: icons/*.png
Copyright: 2009 Frederik Zipp <fzipp@gmx.de>
License: GPL-2

Files: doc/valadoc.1
Copyright: 2010 Evan Nemerson <evan@coeus-group.com>
License: GPL-2

Files: src/libvaladoc/*
Copyright: 2008-2014 Florian Brosch <flo.brosch@gmail.com>
           2008-2014 Didier Villevaloias <ptitjes@free.fr>
           2008-2010 Jürg Billeter <j@bitron.ch>
           2011 Luca Bruno <lethalman88@gmail.com>
License: LGPL-2.1+

Files: src/doclets/*
Copyright: 2007-2009 Jürg Billeter <j@bitron.ch>
           2008-2009 Florian Brosch <flo.brosch@gmail.com>
           2008-2009 Didier Villevaloias <ptitjes@free.fr>
           2010 Luca Bruno <lethalman88@gmail.com>
License: LGPL-2.1+

Files: tests/testrunner.sh
Copyright: 2006-2008 Jürg Billeter <j@bitron.ch>
License: LGPL-2.1+

Files: src/vapi/libgvc.vapi
Copyright: 2009 Martin Olsson <martin@minimum.se>
License: LGPL-2.1+

License: GPL-2
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 On Debian GNU/Linux systems, the complete text of the Lesser GNU General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any purpose with
 or without fee is hereby granted, provided that the above copyright notice and this
 permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD
 TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN
 NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
